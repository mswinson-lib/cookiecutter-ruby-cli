# rubocop:disable Style/DocumentationMethod
module {{cookiecutter.project_klass}}
  # encapsulates shell command
  class Command < Thor
    no_commands do
      def configure(config)
      end
    end

    private

    # initialize command
    # @api private
    def initialize(args = [], local_options = {}, config = {})
      configure(config)
      super
    end
  end
end
