# rubocop:disable Metrics/MethodLength, Metrics/LineLength
module {{cookiecutter.project_klass}}
  module CLI
    # shell runner
    class Runner
      # initializes runner
      # @api private
      # @param argv [Array] command args
      # @param stdin [IO] input stream
      # @param stdout [IO] output stream
      # @param stderr [IO] error stream
      # @param kernel [Kernel] kernel class
      def initialize(argv, stdin = STDIN, stdout = STDOUT, stderr = STDERR, kernel = Kernel)
        @argv = argv
        @stdin = stdin
        @stdout = stdout
        @stderr = stderr
        @kernel = kernel
      end

      # runs shell
      # @api public
      # @raise StandardError
      # @raise SystemExit
      def execute!
        exit_code = begin
          $stderr = @stderr
          $stdin = @stdin
          $stdout = @stdout
          {{cookiecutter.project_klass}}::CLI::Shell.start(@argv)
          0
        rescue StandardError => e
          b = e.backtrace
          @stderr.puts("#{b.shift}: #{e.message} (#{e.class})")
          @stderr.puts(b.map { |s| "\tfrom #{s}" }.join("\n"))
          1
        rescue SystemExit => e
          e.status
        ensure
          $stderr = STDERR
          $stdin = STDIN
          $stdout = STDOUT
        end

        @kernel.exit(exit_code)
      end
    end
  end
end
