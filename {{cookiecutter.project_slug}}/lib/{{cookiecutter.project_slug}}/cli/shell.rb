# rubocop:disable Style/DocumentationMethod
module {{cookiecutter.project_klass}}
  module CLI
    # Command line Shell
    class Shell < Command
      desc 'version', 'display current version'
      def version
        $stdout.puts({{cookiecutter.project_klass}}::VERSION)
      end
    end
  end
end
