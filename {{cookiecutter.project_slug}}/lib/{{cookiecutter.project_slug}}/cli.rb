require 'bundler'
Bundler.setup(:default)
require 'thor'
require '{{cookiecutter.project_slug}}'
require '{{cookiecutter.project_slug}}/command'
require '{{cookiecutter.project_slug}}/cli/shell'
require '{{cookiecutter.project_slug}}/cli/runner'
