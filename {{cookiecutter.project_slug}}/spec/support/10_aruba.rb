require 'aruba/api'
require 'aruba/in_process'
require 'aruba/reporting'

Aruba.configure do |config|
  config.command_launcher = :in_process
  config.main_class = {{cookiecutter.project_klass}}::CLI::Runner
end

RSpec.configure do |config|
  config.include Aruba::Api

  config.before(:each) do
    restore_env
    setup_aruba
  end
end
