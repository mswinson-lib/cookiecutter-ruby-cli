require 'spec_helper'

describe('{{cookiecutter.project_slug}}', 'version') do
  it('outputs the current version number') do
    run('{{cookiecutter.project_slug}} version')

    expect(last_command_started).to have_exit_status(0)
    expect(last_command_started).to have_output({{cookiecutter.project_klass}}::VERSION)
  end
end
